/**
 * Leer un número entena de dos digitos y determinar si es primo y además si es negativo.
 */

const isPrimo=(numero)=>{
    let is_primo = true;
    for (let i=2; i<numero; i++){
        if(numero%i===0){
            return false;
        }
    }
    return is_primo;
}

const isNegativo=(numero)=>{
    let negativo = false;
    if(numero<0){
        negativo = true;
    }
    return negativo;
}

let numero = -67;
if(typeof numero === 'number' ){
    newNumero = Math.abs(numero)
    if(newNumero>=10 && newNumero<100){
        is_primo = isPrimo(newNumero);
        is_negativo = isNegativo(numero);
        console.log(is_primo);
        if(is_primo && is_negativo){
            console.log(`El numero ${numero} es primo y negativo`);
        }else if (is_primo){
            console.log(`El numero ${numero} es primo y positivo`);
        }else if (is_negativo){
            console.log(`El numero ${numero} es no primo y negativo`);
        }else{
            console.log(`El numero ${numero} es no primo y positivo`);
        }


    }else{
        console.log("El numero no cumple los requisitos");
    }
}else{
    console.log("No es un numero");
}