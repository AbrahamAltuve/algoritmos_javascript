/**
 * Leer un número entena de dos digitos menor gue 20 y determinar si es primo.
 */
const isPrimo=(numero)=>{
    let is_primo = true;
    for (let i=2; i<numero; i++){
        if(numero%i===0){
            return false;
        }
    }
    return is_primo;
}

let numero = 17;
if(typeof numero === 'number' ){
    if(numero>=10 && numero<20){
        is_primo = isPrimo(numero);
        if(is_primo){
            console.log(`El numero ${numero} es primo`);
        }else{
            console.log(`El numero ${numero} no es primo`);
        }
    }else{
        console.log("El numero no cumple los requisitos");
    }
}else{
    console.log("No es un numero");
}