/**
 * Leer 10 números enteros, almacenarlos en un vector y determinar en qué 
 * posiciones se encuentran los números terminados en O
 */

let numeros = [10,2,3,40,5,6,45,80,9,10];
let indexs = [];

for (let i = 0; i < numeros.length;i++) {
    if(numeros[i]%10===0){
        indexs.push(i);
    }
}

console.log(`Los numeros terminados en 0 se encuentran en ${indexs}`);
