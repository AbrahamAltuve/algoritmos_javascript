/** 
 * Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 
 * **/

let numero = 1232;
if(typeof numero === 'number' ){
    if(numero>=1000 && numero<10000){
        let parteInicio = parseInt(numero/100%10);
        let parteFinal = numero%10;
        if(parteInicio === parteFinal){
            console.log("El segundo numero y el ultimo numero son iguales");
        }else{
            console.log("El segundo numero y el ultimo numero no son iguales");
        }
    }else{
        console.log("El numero no cumple los requisitos");
    }
}else{
    console.log("No es un numero");
}