/** 
 * Leer un número entero y determinar cuantas veces tiene el digito 1
 * **/

let numero = 2123312131;
if(typeof numero === 'number' ){
    let cadenaNumero = numero.toString();
    let cant_1 = 0;
    for(var i = 0; i < cadenaNumero.length; i++) {
        if(cadenaNumero.charAt(i) == '1') {
            cant_1++;
        }
    } 
    console.log(`El 1 aparece ${cant_1} veces en ${numero}`);
}else{
    console.log("No es un numero");
}