/**
 * Leer un número entero y mostrar en pantalla su tabla de multiplicar
 */

const tablaMultiplicar = (n)=>{
    console.log(`Tabla del ${n}`);
    for(var j = 1;j<=10;j++){
        console.log(`${n}x${j}=${n*j}`)
    }
} 

 console.log(tablaMultiplicar(14));