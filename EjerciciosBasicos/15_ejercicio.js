/**
 * Leer 10 números enteros, almacenarlos en un vector y 
 * determinar en qué posiciones se encuentra el número mayor. 
 */


let numeros = [1,2,3,4,5,6,45,8,9,10];
let indexMayor = 0;
let numeroMayor = numeros[0];

for (let i = 0; i < numeros.length;i++) {
    if(numeros[i]>numeroMayor){
        indexMayor=i;
        numeroMayor=numeros[i];
    }
}

console.log(`El numero mayor es ${numeroMayor} en la posicion ${indexMayor}`);

