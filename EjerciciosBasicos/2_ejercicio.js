/** 
 * Leer un número entero de dos digitos y determinar 
 * a cuánto es igual la suma de sus digitos.
 * **/

let numero = 24;
if(typeof numero === 'number' ){
    if(numero>=10 && numero<100){
        let parteInicio = parseInt(numero/10);
        let parteFinal = numero%10;
        let suma = parteInicio+parteFinal
        console.log(`La suma es ${suma}`);
    }else{
        console.log("El numero no cumple los requisitos");
    }
}else{
    console.log("No es un numero");
}