/** 
 * Leer un número entero de dos digitos y 
 * determinar si ambos digitos son pares.
 * **/

const is_par = (n)=>{
    let par = false;
    if(n%2===0){
        par = true;
    }
    return par;
}

let numero = 24;
if(typeof numero === 'number' ){
    if(numero>=10 && numero<100){
        let parteInicio = parseInt(numero/10);
        let parteFinal = numero%10;
        if(is_par(parteInicio)&&is_par(parteFinal)){
            console.log("Ambos numeros son pares");
        }else if(is_par(parteInicio) || is_par(parteFinal)){
            console.log("Uno de los numeros es par");
        }else{
            console.log("Ninguno de los numeros es par");
        }
    }else{
        console.log("El numero no cumple los requisitos");
    }
}else{
    console.log("No es un numero");
}