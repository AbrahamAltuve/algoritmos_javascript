/** 
 * Leer tres numeros enteros y determinar si el último digito. de los tres numeros es igual.  
 * **/

let numero1 = 1436;
let numero2 = 1236;
let numero3 = 1236;
if(typeof numero1 === 'number' && typeof numero2 === 'number' && typeof numero3 === 'number'){
    if((numero1>=1000 && numero1<10000) && (numero2>=1000 && numero2<10000) && (numero3>=1000 && numero3<10000)){
        let finalNumero1 = numero1%10;
        let finalNumero2 = numero2%10;
        let finalNumero3 = numero3%10;
        if(finalNumero1 === finalNumero2 && finalNumero1 === finalNumero3){
            console.log("Los numeros son iguales");
        }else{
            console.log("Los numeros no son iguales");
        }
    }else{
        console.log("Alguno de los numeros no cumple los requisitos");
    }
}else{
    console.log("Alguno de los tres datos no es un numero");
}