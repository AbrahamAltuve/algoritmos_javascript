/**
 * Leer un número entero y determinar a cuánto es igual la suma de sus digitos.
 */

const calcularSumaPorDigitos = (numero)=>{
    if(numero>=10){
        let final = numero%10;
        let resto = parseInt(numero/10);
        return final+calcularSumaPorDigitos(resto);
    }else{
        return numero;
    }
 }

let numero = 2367;
if(typeof numero === 'number' ){
    let suma = calcularSumaPorDigitos(numero)
    console.log(`La suma es ${suma}`);

}else{
    console.log("No es un numero");
}