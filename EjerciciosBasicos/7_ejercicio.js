/** 
 * Leer un número entero de dos digitos y determinar si los dos digitos son iguales.
 * **/

let numero = 22;
if(typeof numero === 'number' ){
    if(numero>=10 && numero<100){
        let parteInicio = parseInt(numero/10);
        let parteFinal = numero%10;
        if(parteInicio === parteFinal){
            console.log("Los numeros son iguales");
        }else{
            console.log("Los numeros no son iguales");
        }
    }else{
        console.log("El numero no cumple los requisitos");
    }
}else{
    console.log("No es un numero");
}