/**
 * Leer dos numeres enteros y determinar si la diferencia entre los dos es 
 * un número divisor exacto de alguno de los dos números.
 */

const isDivisor=(num,divisor)=>{
    is_divisor = false;
    if(num%divisor===0){
        is_divisor=true;
    }
    return is_divisor;
 }

let numero1 = 6;
let numero2 = 2;
if(typeof numero1 === 'number' && typeof numero2 === 'number'){
    
    diferencia = numero1-numero2;
    if(isDivisor(numero1,diferencia)&& isDivisor(numero2,diferencia)){
        console.log(`${diferencia} es divisor de ${numero1} y de ${numero2}`);
    }else if(isDivisor(numero1,diferencia)||isDivisor(numero2,diferencia)){
        console.log(`${diferencia} es divisor de ${numero1} o de ${numero2}`);
    }else{
        console.log(`${diferencia} no es divisor de ${numero1} ni de ${numero2}`);
    }
    
}else{
    console.log("Alguno de los tres datos no es un numero");
}