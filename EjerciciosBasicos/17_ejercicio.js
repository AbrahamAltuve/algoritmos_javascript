/**
 * Leer 10 números enteros, almacenados en un vcetor y determinar 
 * cuántas veces está repetido el mayor
 */

const calcularNumerMayor=(numeros)=>{
    let numeroMayor = numeros[0];

    for (let i = 0; i < numeros.length;i++) {
        if(numeros[i]>numeroMayor){
            numeroMayor=numeros[i];
        }
    }
    return numeroMayor;
}

calcularRepeticionNumero=(numero,numeros)=>{
    let vecesRepetido = 0;
    for (let i = 0; i < numeros.length;i++) {
        if(numeros[i]===numero){
            vecesRepetido++;
        }
    }
    return vecesRepetido;
}

let numeros = [1,45,3,4,5,6,45,8,45,10];
numeroMayor=calcularNumerMayor(numeros);
console.log(numeroMayor);
cantidadRepeticiones = calcularRepeticionNumero(numeroMayor,numeros);
console.log(cantidadRepeticiones);