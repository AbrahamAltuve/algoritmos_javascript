/** 
 * Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.
 * **/

const is_multiplo = (n1,n2)=>{
    let multiplo = false;
    if(n1%n2===0){
        multiplo = true;
    }
    return multiplo;
}

let numero = 28;
if(typeof numero === 'number' ){
    if(numero>=10 && numero<100){
        let parteInicio = parseInt(numero/10);
        let parteFinal = numero%10;
        if(is_multiplo(parteInicio,parteFinal) && is_multiplo(parteFinal,parteInicio)){
            console.log("Ambos numeros son multiplos del otro");
        }else if(is_multiplo(parteInicio,parteFinal) || is_multiplo(parteFinal,parteInicio)){
            console.log("Uno de los numeros es multiplo del otro");
        }else{
            console.log("Ninguno de los numeros es multiplo del otro");
        }
    }else{
        console.log("El numero no cumple los requisitos");
    }
}else{
    console.log("No es un numero");
}