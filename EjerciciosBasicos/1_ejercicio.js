/**
 * Leer un número entero y determinar si es negativo.
**/
let numero = -24;
if(typeof numero === 'number' ){
    if(numero<0){
        console.log("El numero es negativo");
    }else{
        console.log("El numero no es negativo");
    }
}else{
    console.log("No es un numero");
}

