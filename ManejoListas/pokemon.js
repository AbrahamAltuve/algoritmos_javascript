/**
 * Mostrar la cantidad de pokemones agrupados por orden alfabetico, ejempo:
 * Cantidad de pokemones que inician con A: 45
 * Cantidad de pokemones que inician con B: 20
 */
const pokemonNames = require('poke-names');
const allPokemonNames = pokemonNames.all.sort();


const agruparPokemon=()=>{
    pokemonXLetra = [];
    for(let pokemon of allPokemonNames){
        letra = pokemon[0];
        if(pokemonXLetra.find(p => p.letra==letra)){
            pokemonXLetra.map(p => {
                if(p.letra==letra){p.cantidad++}});         
        }else{
            pokemonObjeto = {};
            pokemonObjeto.letra=letra;
            pokemonObjeto.cantidad=1;
            pokemonXLetra.push(pokemonObjeto);
        }
        
        
        
    }
    return pokemonXLetra;
}

const formatoPokemon=(pokemones)=>{
    pokemones.map(pokemon => {
        console.log(`Cantidad de pokemones que inician con ${pokemon.letra}: ${pokemon.cantidad}`)
    });
}

let pokemonesXLetra = agruparPokemon();
formatoPokemon(pokemonXLetra);